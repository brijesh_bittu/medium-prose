"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const overlaynode_1 = __importDefault(require("./overlaynode"));
const field_1 = __importDefault(require("./field"));
const OVERLAY_FORM_CLASS = 'Prosemirror__overlay-form';
let count = 0;
function getID() {
    const lastID = count++;
    return `Prosemirror__input-id__${lastID}`;
}
class OverlayForm extends overlaynode_1.default {
    constructor(options) {
        super();
        this.options = options;
        this.onSubmit = (ev) => {
            ev.preventDefault();
            const { onSubmit } = this.options;
            let isValid = true;
            const values = {};
            this.fields.forEach(field => {
                if (field.isValid()) {
                    values[field.name] = field.value;
                    return;
                }
                isValid = false;
                field.focus();
            });
            if (!isValid) {
                return;
            }
            onSubmit(this.editor, values);
        };
        this.onKeyDown = (ev) => {
            const { closeOnEscape } = this.options;
            if (ev.which === 27 && closeOnEscape) {
                this.hide();
            }
        };
        this.onEditorFocus = () => {
            this.hide();
        };
        this.form = document.createElement('form');
        if (options.ariaLabel) {
            this.form.setAttribute('aria-label', options.ariaLabel);
        }
        this.form.className = OVERLAY_FORM_CLASS;
        this.fields = options.fields.map(this.createField);
        this.dom.appendChild(this.form);
        this.fields.forEach(field => {
            this.form.appendChild(field.dom);
        });
        if (options.showSubmitButton) {
            this.form.appendChild(this.createSubmitButton());
        }
        this.form.addEventListener('submit', this.onSubmit);
    }
    createField(fieldOptions) {
        return new field_1.default(fieldOptions, getID());
    }
    createSubmitButton() {
        const button = document.createElement('button');
        button.className = `${OVERLAY_FORM_CLASS}__submit-btn`;
        button.type = 'submit';
        button.textContent = this.options.submitLabel || 'Done';
        return button;
    }
    setValues(values) {
        this.fields.forEach(field => {
            const val = values[field.name];
            if (val !== undefined) {
                field.value = val;
            }
        });
    }
    getDom() {
        return super.dom;
    }
    update(view) {
        this.setEditor(view);
    }
    show(view) {
        this.setEditor(view);
        super.show();
        this.form.addEventListener('keydown', this.onKeyDown);
        this.editor.dom.addEventListener('focus', this.onEditorFocus);
        if (this.fields.length) {
            this.fields[0].focus();
        }
    }
    hide() {
        this.fields.forEach(f => {
            f.hideError();
            f.value = '';
        });
        this.form.removeEventListener('keydown', this.onKeyDown);
        this.editor.dom.removeEventListener('focus', this.onEditorFocus);
        super.hide();
    }
    destroy() {
        this.fields.forEach(field => field.destroy());
        this.form.removeEventListener('submit', this.onSubmit);
        this.form.removeEventListener('keydown', this.onKeyDown);
        this.editor.dom.removeEventListener('focus', this.onEditorFocus);
        this.dom.removeChild(this.form);
        super.destroy();
    }
}
exports.default = OverlayForm;
