"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ExtensionType;
(function (ExtensionType) {
    ExtensionType["Mark"] = "mark";
    ExtensionType["Node"] = "node";
    ExtensionType["Plugin"] = "plugin";
})(ExtensionType = exports.ExtensionType || (exports.ExtensionType = {}));
