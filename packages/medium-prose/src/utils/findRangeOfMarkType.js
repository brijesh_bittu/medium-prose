"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_state_1 = require("prosemirror-state");
function findRangeOfMarkType(markType, doc, selection) {
    // if (!selection.empty) {
    //   return {
    //     selection,
    //     mark: null,
    //   };
    // }
    const { $from, from: cursorPos } = selection;
    const startLimitPos = $from.before($from.depth) + 1;
    const endLimitPos = $from.after($from.depth);
    let mark = null;
    let startPos = cursorPos;
    let endPos = cursorPos;
    for (; startPos >= startLimitPos;) {
        const node = doc.nodeAt(startPos);
        if (!node) {
            break;
        }
        const foundMark = markType.isInSet(node.marks);
        if (!node || !foundMark || (mark && foundMark !== mark)) {
            if (startPos !== cursorPos) {
                startPos++;
            }
            break;
        }
        startPos--;
        mark = foundMark;
    }
    for (; endPos <= endLimitPos;) {
        const node = doc.nodeAt(endPos);
        if (!node) {
            break;
        }
        const foundMark = markType.isInSet(node.marks);
        if (!foundMark || (mark && foundMark !== mark)) {
            break;
        }
        endPos++;
        mark = foundMark;
    }
    return {
        selection: selection.empty ? prosemirror_state_1.TextSelection.create(doc, startPos, endPos) : selection,
        mark,
    };
}
exports.default = findRangeOfMarkType;
