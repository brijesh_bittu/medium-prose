import { manager } from 'medium-prose'
import findRangeOfMarkType from 'medium-prose/lib/utils/findRangeOfMarkType';

import 'prosemirror-view/style/prosemirror.css';
import 'medium-prose/src/extensions/nodes/code.css';
import 'medium-prose/src/extensions/code.css';
import 'medium-prose/src/extensions/nodes/blockquote.css';
import 'medium-prose/src/extensions/nodes/paragraph.css';
import 'medium-prose/src/views/field.css';
import 'medium-prose/src/views/overlayform.css';

const node = document.getElementById('root');

const editor = manager.mount(node!);
editor.focus();

window.editor = editor;
window.manager = manager;
window.findRangeOfMarkType = findRangeOfMarkType;

declare global {
  interface Window {
    editor: any;
    manager: any;
    findRangeOfMarkType: any;
  }
}
