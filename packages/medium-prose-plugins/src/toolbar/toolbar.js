"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const preact_1 = require("preact");
const constants_1 = require("../constants");
class ToolbarItem extends preact_1.Component {
    constructor() {
        super(...arguments);
        this.handleClick = (ev) => {
            console.log(ev);
        };
    }
    render(props) {
        const { title } = props;
        return (<button className={`${constants_1.TOOLBAR_BASE_CLASS}__btn-item`} onClick={this.handleClick}>
        {title}
      </button>);
    }
}
class Toolbar extends preact_1.Component {
    getItems(items, type) {
        const toolbarItems = [];
        items.forEach((key) => {
            toolbarItems.push({
                title: key.split('_').join(' '),
                action: key,
                type,
            });
        });
        return toolbarItems;
    }
    constructor(props) {
        super(props);
        const { marks, nodes } = props.schema.spec;
        this.state = {
            buttons: [
                this.getItems(marks, 'mark'),
                this.getItems(nodes, 'node'),
            ],
        };
    }
    render({ show }, { buttons }) {
        if (!show) {
            return null;
        }
        const className = `${constants_1.TOOLBAR_BASE_CLASS} ${constants_1.TOOLBAR_BASE_CLASS}--shown`;
        const [marks, nodes] = buttons;
        return (<div className={className}>
        {nodes.map(node => <ToolbarItem {...node}/>)}
        {marks.map(mark => <ToolbarItem {...mark}/>)}
      </div>);
    }
}
exports.default = Toolbar;
