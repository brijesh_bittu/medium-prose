"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function findParentPosFor($position, predicate) {
    for (let i = $position.depth; i > 0; i--) {
        const node = $position.node(i);
        if (predicate(node)) {
            return i > 0 ? $position.before(i) : 0;
        }
    }
}
exports.default = findParentPosFor;
