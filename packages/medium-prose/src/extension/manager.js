"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_model_1 = require("prosemirror-model");
const prosemirror_state_1 = require("prosemirror-state");
const prosemirror_view_1 = require("prosemirror-view");
const prosemirror_keymap_1 = require("prosemirror-keymap");
const base_1 = require("./base");
const baseplugin_1 = require("./baseplugin");
const defaultOptions = {
    useDefaultExtensions: true,
    extensions: [],
};
class ExtensionManager {
    constructor(options = defaultOptions) {
        this.options = options;
        this.keyMapDescriptions = [];
        this.keyMaps = {};
        this.init();
    }
    init() {
        const marks = {};
        const nodes = {};
        const plugins = [];
        const defaultExtensions = [
            new baseplugin_1.BaseKeymapPlugin(),
            new baseplugin_1.HistoryPlugin(),
        ];
        const { extensions } = this.options;
        const fullList = defaultExtensions.concat(extensions ? extensions : []);
        fullList.forEach((extension) => {
            const markSpec = this.getMarks(extension);
            if (markSpec) {
                marks[extension.name] = markSpec;
            }
            const nodeSpec = this.getNodes(extension);
            if (nodeSpec) {
                nodes[extension.name] = nodeSpec;
            }
            const tmpPlugins = this.getPlugins(extension);
            if (tmpPlugins && tmpPlugins.length) {
                plugins.push(...tmpPlugins);
            }
        });
        this.schema = new prosemirror_model_1.Schema({
            marks,
            nodes,
        });
        this.createKeyMaps(this.schema, fullList);
        this.state = prosemirror_state_1.EditorState.create({
            schema: this.schema,
            plugins: [
                ...plugins,
                prosemirror_keymap_1.keymap(this.keyMaps),
            ],
        });
    }
    getMarks(extension) {
        if (extension.type !== base_1.ExtensionType.Mark) {
            return null;
        }
        const spec = extension.getSchema && extension.getSchema();
        if (!spec) {
            return null;
        }
        return spec;
    }
    getNodes(extension) {
        if (extension.type !== base_1.ExtensionType.Node) {
            return null;
        }
        const spec = extension.getSchema && extension.getSchema();
        if (!spec) {
            return null;
        }
        return spec;
    }
    getPlugins(extension) {
        const plugins = extension.getPlugins && extension.getPlugins();
        if (!plugins || !plugins.length) {
            return null;
        }
        return plugins;
    }
    createKeyMaps(schema, extensions) {
        extensions.forEach((extension) => {
            let keyMaps;
            if (extension.type === base_1.ExtensionType.Plugin) {
                keyMaps = extension.getKeyMaps && extension.getKeyMaps();
            }
            else {
                const key = extension.type === base_1.ExtensionType.Node ? 'nodes' : 'marks';
                const extensionSchema = schema[key][extension.name];
                keyMaps = extension.getKeyMaps && extension.getKeyMaps({ type: extensionSchema, schema });
            }
            if (!keyMaps || !keyMaps.length) {
                return;
            }
            keyMaps.forEach((keyMap) => {
                this.keyMaps[keyMap.key] = keyMap.handler;
                this.keyMapDescriptions.push({
                    key: keyMap.key,
                    description: keyMap.description || 'No description',
                });
            });
        });
    }
    mount(dom) {
        const editor = new prosemirror_view_1.EditorView(dom, {
            state: this.state,
        });
        this.editor = editor;
        editor.setProps({
            state: this.state,
            attributes: {
                'aria-label': 'Press Mod-? to see all available shortcuts',
            },
        });
        return editor;
    }
}
exports.default = ExtensionManager;
