"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NODE_TYPE = {
    Block: 'block',
    Inline: 'inline',
    Text: 'text',
};
exports.BLOCK_TYPE = {
    Paragraph: 'paragraph',
    BlockQuote: 'blockquote',
    HR: 'horizontal_rule',
    Heading: 'heading',
    CodeBlock: 'code_block',
    Text: 'text',
    LineBreak: 'line_break',
};
exports.MARK_TYPE = {
    Link: 'link',
    EM: 'em',
    Strong: 'strong',
    Code: 'code',
};
function oneOrMore(block) {
    return `(${block})+`;
}
exports.oneOrMore = oneOrMore;
function zeroOrMore(block) {
    return `(${block})*`;
}
exports.zeroOrMore = zeroOrMore;
function zeroOrOne(block) {
    return `(${block})?`;
}
exports.zeroOrOne = zeroOrOne;
function anyOf(...args) {
    return args.join(' | ');
}
exports.anyOf = anyOf;
exports.TOOLBAR_BASE_CLASS = 'Prosemirror__floating-toolbar';
