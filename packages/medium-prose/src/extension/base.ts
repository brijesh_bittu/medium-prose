import {
  MarkSpec,
  NodeSpec,
  Node,
  Mark,
  MarkType,
  NodeType,
  Schema,
} from 'prosemirror-model';
import {
  EditorState,
  Transaction,
  Plugin,
} from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';

export enum ExtensionType {
  Mark = 'mark',
  Node = 'node',
  Plugin = 'plugin',
}

export type KeyHandler = (state: EditorState, dispatch?: (tr: Transaction) => void, view?: EditorView) => boolean;

export interface IKeymapOptions {
  type: MarkType | NodeType,
  schema: Schema,
}

export interface IExtension {
  name: string;
  type: ExtensionType;
  isFallbackNode?: boolean;
  getSchema?(): MarkSpec | NodeSpec;
  getPlugins?(): Plugin[];
  getKeyMaps?(options?: IKeymapOptions): {
    // The key combination, ie, Mod-B for bold (Mod translates to Command on Mac
    // and CTRL on other OSes.
    key: string;
    // A short description of what this key press does to populate a global list
    // to present to users if required.
    description?: string,
    handler: KeyHandler,
  }[];
}
