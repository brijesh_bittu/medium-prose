"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = require("../../extension/base");
class Text {
    constructor() {
        this.type = base_1.ExtensionType.Node;
        this.name = 'text';
    }
    getSchema() {
        return {
            group: 'inline',
        };
    }
}
exports.default = Text;
