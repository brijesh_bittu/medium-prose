import {
  DOMOutputSpecArray,
  MarkType,
  NodeSpec,
  NodeType,
} from 'prosemirror-model';

import { IExtension, ExtensionType } from '../../extension/base';
import { toggleWrap } from '../../commands/block';

export default class Blockquote implements IExtension {
  type = ExtensionType.Node;
  name = 'blockquote';

  getSchema() {
    return {
      content: 'block*',
      group: 'block',
      defining: true,
      draggable: false,
      parseDOM: [{
        tag: 'blockquote',
      }],
      toDOM() {
        return ['blockquote', 0];
      },
    } as NodeSpec;
  }

  getKeyMaps(options?: { type: MarkType | NodeType }) {
    const { type } = options!;
    return [{
      key: 'Mod->',
      description: 'Toggle block type to Blockquote',
      handler: toggleWrap(type as NodeType),
    }];
  }
}

