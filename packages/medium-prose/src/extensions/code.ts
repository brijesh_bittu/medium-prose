import { DOMOutputSpecArray, Mark, MarkType, NodeType } from 'prosemirror-model';
import { toggleMark } from 'prosemirror-commands';

import { IExtension, ExtensionType } from '../extension/base';

export default class Code implements IExtension {
  type = ExtensionType.Mark;
  name = 'inline_code';

  getSchema() {
    return {
      parseDOM: [
        { tag: 'code' },
      ],
      toDOM(mark: Mark, inline: boolean): DOMOutputSpecArray {
        return ['code', 0];
      },
    };
  }

  getKeyMaps(options?: { type: MarkType | NodeType }) {
    const type = options!.type;

    return [{
      key: 'Mod-`',
      description: 'Toggle selected text to monospaced text.',
      handler: toggleMark(type as MarkType),
    }];
  }
}
