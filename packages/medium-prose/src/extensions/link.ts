import { DOMOutputSpecArray, Mark, NodeType, MarkType, MarkSpec } from 'prosemirror-model';
import { EditorState, Transaction, Plugin } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import { toggleMark } from 'prosemirror-commands';

import findRangeOfMarkType from '../utils/findRangeOfMarkType';
import { IKeymapOptions, IExtension, ExtensionType } from '../extension/base';
import OverlayForm from '../views/overlayform';

interface ILinkOptions {
  validateLink?(value: string): string | null;
}

export default class Link implements IExtension {
  pluginView: OverlayForm;
  markType: MarkType;
  type = ExtensionType.Mark;
  name = 'link';

  constructor(options: ILinkOptions = {}) {
    this.pluginView = new OverlayForm({
      ariaLabel: 'Add or update link of the selected text',
      closeOnEscape: true,
      fields: [{
        name: 'href',
        type: 'text',
        label: 'Enter URL (*)',
        validate: options.validateLink ? options.validateLink : undefined,
      }, {
        name: 'title',
        type: 'text',
        label: 'Title to add to the link',
      }],
      onSubmit: this.onSubmit,
      showSubmitButton: true,
    });
  }

  linkHandler(linkType: MarkType, remove = false) {
    this.markType = linkType;

    return (state: EditorState, dispatch?: (tr: Transaction) => void, view?: EditorView) => {
      const { selection } = state;
      let { parent } = selection.$from;
      const { selection: linkSelection, mark } = findRangeOfMarkType(linkType, state.doc, selection);

      if (remove) {
        if (!mark || linkSelection.empty) {
          return false;
        }

        const transaction = state.tr.removeMark(linkSelection.from, linkSelection.to, linkType);
        if (dispatch) {
          dispatch(transaction);
        }
        return true;
      }

      if (!selection.empty || !linkSelection.empty) {
        if (mark) {
          this.pluginView.setValues(mark.attrs);
        }
        this.pluginView.show(view!);
        return true;
      }

      return false;
    };
  }

  onSubmit = (editor: EditorView, values: { [key: string]: string }) => {
    this.pluginView.hide();
    const { state } = editor;
    const { selection } = state;
    let tr = state.tr;
    let selectionToUse = selection;

    if (selection.empty) {
      const { selection: linkSelection } = findRangeOfMarkType(this.markType, state.doc, selection);
      selectionToUse = linkSelection;
    }

    if (selectionToUse.empty) {
      return;
    }

    if (!values.href) {
      tr = tr.removeMark(selectionToUse.from, selectionToUse.to, this.markType);
    } else {
      tr.addMark(selectionToUse.from, selectionToUse.to, this.markType.create(values));
    }

    if (tr === state.tr) {
      return;
    }

    editor.dispatch(tr);
  };

  getSchema() {
    const schema: MarkSpec = {
      attrs: {
        href: {
          default: null,
        },
        title: {
          default: '',
        },
      },
      inclusive: false,
      parseDOM: [
        {
          tag: 'a[href]',
          getAttrs(node: string | Node) {
            if (typeof node === 'string') {
              return null;
            }

            return {
              href: (node as Element).getAttribute('href') || '',
              title: (node as Element).getAttribute('title') || '',
            };
          },
        },
      ],
      toDOM(mark: Mark, inline: boolean): DOMOutputSpecArray {
        return ['a', {
          ...mark.attrs,
          rel: 'noopener noreferrer nofollow',
        }, 0];
      },
    };

    return schema;
  }

  getKeyMaps(options?: { type: MarkType | NodeType }) {
    const { type } = options!;
    return [{
      key: 'Mod-k',
      description: 'Add link to selected text or update data if cursor is inside a link',
      handler: this.linkHandler(type as MarkType),
    }, {
      key: 'Mod-Shift-K',
      description: 'Remove link from text if cursor is inside a link',
      handler: this.linkHandler(type as MarkType, true),
    }];
  }

  getPlugins() {
    const that = this;
    return [
      new Plugin({
        view(editorView: EditorView) {
          return that.pluginView;
        },
        props: {
          handleClick(view, pos, ev) {
            if (!(ev.ctrlKey && ev.altKey)) {
              return false;
            }

            const node = view.state.doc.nodeAt(pos);

            if (!node || !node.marks.length) {
              return false;
            }

            const mark = node.marks.find((m) => m.type === that.markType);

            if (!mark) {
              return false;
            }

            ev.preventDefault();
            ev.stopPropagation();
            window.open(mark.attrs.href);
            return true;
          }
        },
      }),
    ];
  }
}
