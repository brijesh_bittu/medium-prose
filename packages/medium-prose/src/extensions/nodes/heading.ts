import {
  DOMOutputSpecArray,
  MarkType,
  NodeSpec,
  NodeType,
  Node,
} from 'prosemirror-model';
import { setBlockType } from 'prosemirror-commands';

import { IExtension, ExtensionType } from '../../extension/base';
import { toggleWrap } from '../../commands/block';

interface IHeadingOptions {
  levels: number[];
}

export default class Heading implements IExtension {
  type = ExtensionType.Node;
  name = 'heading';

  constructor(public options: IHeadingOptions = { levels: [1, 2, 3, 4, 5, 6] }) {}

  getSchema() {
    const { levels } = this.options;

    return {
      attrs: {
        level: {
          default: levels[0],
        },
      },
      content: 'inline*',
      group: 'block',
      defining: true,
      draggable: false,
      parseDOM: levels.map((level: number) => ({
        tag: `h${level}`,
        attrs: { level },
      })),
      toDOM(node: Node) {
        const { level } = node.attrs;
        return [`h${level}`, 0];
      },
    } as NodeSpec;
  }

  getKeyMaps(options?: { type: MarkType | NodeType }) {
    const { type } = options!;
    const { levels } = this.options;

    return levels.map((level: number) => {
      return {
        key: `Mod-Shift-${level}`,
        description: `Toggle block type to h${level}`,
        handler: setBlockType(type as NodeType, { level }),
      };
    });
  }
}

