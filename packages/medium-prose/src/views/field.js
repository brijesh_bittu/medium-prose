"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const defaultOptions = {
    type: 'text',
    name: 'text',
};
const INPUT_CLASS = 'Prosemirror__form-input';
const WRAPPER_CLASS = 'Prosemirror__form-field';
class Field {
    constructor(options, id) {
        this.errorShown = false;
        this.dom = document.createElement('div');
        this.onInputChange = () => {
            this.hideError();
        };
        this.options = Object.assign({}, defaultOptions, options);
        this.input = this.createInput(id);
        if (this.options.label) {
            const label = this.createLabel(this.options.label, id);
            this.dom.appendChild(label);
        }
        this.errorNode = this.createErrorNode(id);
        this.dom.className = WRAPPER_CLASS;
        this.dom.appendChild(this.input);
        this.dom.appendChild(this.errorNode);
    }
    createErrorNode(id) {
        const node = document.createElement('span');
        node.style.display = 'none';
        node.className = `${INPUT_CLASS}__error`;
        if (id) {
            node.id = `${id}_error`;
        }
        return node;
    }
    createInput(id) {
        const { type, placeholder, required } = this.options;
        const input = document.createElement('input');
        input.type = type;
        if (placeholder) {
            input.placeholder = placeholder || '';
        }
        input.required = !!required;
        input.autocomplete = 'off';
        input.addEventListener('input', this.onInputChange);
        if (id !== undefined) {
            input.id = id;
            input.setAttribute('aria-describedby', `${id}_error`);
        }
        return input;
    }
    createLabel(label, inputId) {
        const labelNode = document.createElement('label');
        labelNode.className = `${INPUT_CLASS} ${this.options.type}`;
        const text = document.createTextNode(label);
        labelNode.appendChild(text);
        if (inputId !== undefined) {
            labelNode.htmlFor = inputId;
        }
        return labelNode;
    }
    removeListeners() {
        this.input.removeEventListener('input', this.onInputChange);
    }
    focus() {
        this.input.focus();
    }
    get value() {
        return this.input.value;
    }
    set value(val) {
        this.input.value = val;
    }
    get name() {
        return this.options.name;
    }
    isValid() {
        const { validate } = this.options;
        if (validate) {
            const val = validate(this.input.value);
            if (val !== null) {
                this.errorNode.textContent = val;
                this.input.setAttribute('aria-invalid', 'true');
                if (!this.errorShown) {
                    this.errorNode.style.display = 'initial';
                    this.errorShown = true;
                }
                return false;
            }
        }
        return true;
    }
    hideError() {
        if (!this.errorShown) {
            return;
        }
        this.errorShown = false;
        this.input.removeAttribute('aria-invalid');
        this.errorNode.style.display = 'none';
        this.errorNode.textContent = '';
    }
    destroy() {
        this.removeListeners();
    }
}
exports.default = Field;
