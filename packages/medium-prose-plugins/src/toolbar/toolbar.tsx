import OrderedMap from 'orderedmap';
import { h, Component } from 'preact';
import { Schema, MarkSpec, NodeSpec } from 'prosemirror-model';

import { TOOLBAR_BASE_CLASS as BASE_CLASS } from '../constants';

export interface Props {
  show: boolean,
  schema: Schema,
}

interface BtnProps {
  title: string,
  type: 'mark' | 'node';
  action: string,
}

interface State {
  buttons: Array<Array<BtnProps>>;
}

class ToolbarItem extends Component<BtnProps> {
  handleClick = (ev: MouseEvent) => {
    console.log(ev);
  };

  render(props: BtnProps) {
    const { title } = props;
    return (
      <button className={`${BASE_CLASS}__btn-item`} onClick={this.handleClick}>
        {title}
      </button>
    );
  }
}


export default class Toolbar extends Component<Props, State> {
  getItems(items: OrderedMap<NodeSpec | MarkSpec>, type: 'mark' | 'node'): Array<BtnProps> {
    const toolbarItems: Array<BtnProps> = [];

    items.forEach((key) => {
      toolbarItems.push({
        title: key.split('_').join(' '),
        action: key,
        type,
      });
    });

    return toolbarItems;
  }

  constructor(props: Props) {
    super(props);

    const { marks, nodes } = props.schema.spec;

    this.state = {
      buttons: [
        this.getItems(marks as OrderedMap<MarkSpec>, 'mark'),
        this.getItems(nodes as OrderedMap<NodeSpec>, 'node'),
      ],
    };
  }

  render({ show }: Props, { buttons }: State) {
    if (!show) {
      return null;
    }

    const className = `${BASE_CLASS} ${BASE_CLASS}--shown`;
    const [ marks, nodes ] = buttons;

    return (
      <div className={className}>
        {nodes.map(node => <ToolbarItem {...node} />)}
        {marks.map(mark => <ToolbarItem {...mark} />)}
      </div>
    );
  }
}
