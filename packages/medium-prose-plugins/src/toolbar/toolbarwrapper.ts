import { h, render, rerender } from 'preact';
import { EditorView } from 'prosemirror-view';
import { Transaction, StateField, Plugin, EditorState, PluginKey } from 'prosemirror-state';

import { TOOLBAR_BASE_CLASS } from '../constants';
import Toolbar, { Props as ToolbarProps } from './toolbar';

const pluginKey = new PluginKey('floating-toolbar-plugin');

class ToolbarState implements StateField<boolean> {
  init(config: { [key: string]: any }, state: EditorState) {
    return false;
  }

  apply(tr: Transaction, value: boolean, oldState: EditorState, newState: EditorState) {
    if (newState.selection.eq(oldState.selection) || newState.selection.empty) {
      return false;
    }

    return true;
  }
}

class ToolbarWrapper {
  private view: EditorView;
  private key: PluginKey;
  private renderedElement: Element | null = null;
  private container: HTMLElement;

  constructor(key: PluginKey, view: EditorView) {
    this.view = view;
    this.key = key;
    this.container = document.createElement('div');
    this.container.className = `${TOOLBAR_BASE_CLASS}__container`;
  }

  update(view: EditorView, prevState: EditorState) {
    const { state } = view;
    const newState = this.key.getState(state);
    const oldState = this.key.getState(prevState);

    if (newState) {
      this.show();
    } else {
      this.show(false);
    }
  }

  show(show = true) {
    if (!this.renderedElement) {
      this.view.dom.parentElement!.appendChild(this.container);
    }

    const { schema } = this.view.state;
    const props: ToolbarProps = {
      show,
      schema,
    };

    if (!this.renderedElement) {
      this.renderedElement = render(
        h(Toolbar, props),
        this.container,
      );
    } else {
      this.renderedElement = render(
        h(Toolbar, props),
        this.container,
        this.renderedElement,
      );
    }
  }

  destroy() {
    if (this.renderedElement) {
      render(
        null,
        this.container,
        this.renderedElement,
      );
      this.renderedElement = null;
    }
    this.view.dom.parentElement!.removeChild(this.container);
  }
}

export const toolbarPlugin = new Plugin({
  key: pluginKey,
  state: new ToolbarState(),
  view(editorView) {
    return new ToolbarWrapper(pluginKey, editorView);
  },
});

