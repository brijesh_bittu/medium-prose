"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_state_1 = require("prosemirror-state");
const base_1 = require("../../extension/base");
const block_1 = require("../../commands/block");
const findParentPosFor_1 = __importDefault(require("../../utils/findParentPosFor"));
const overlayform_1 = __importDefault(require("../../views/overlayform"));
class Codeblock {
    constructor() {
        this.type = base_1.ExtensionType.Node;
        this.name = 'code';
        this.onSubmit = (view, values) => {
            const { state, dispatch } = view;
            const { $from } = state.selection;
            const pos = findParentPosFor_1.default($from, (node) => node.type === this.nodeType);
            if (pos !== undefined) {
                dispatch(state.tr.setNodeMarkup(pos, undefined, values));
            }
            this.pluginView.hide();
        };
        this.pluginView = new overlayform_1.default({
            fields: [{
                    type: 'text',
                    name: 'language',
                    label: 'Set language for current code block',
                }],
            closeOnEscape: true,
            onSubmit: this.onSubmit,
        });
    }
    getSchema() {
        return {
            content: 'text*',
            marks: '',
            group: 'block',
            code: true,
            defining: true,
            draggable: false,
            attrs: {
                language: {
                    default: '',
                },
            },
            parseDOM: [{
                    tag: 'pre[data-language]',
                    preserveWhitespace: 'full',
                    getAttrs(node) {
                        if (typeof node === 'string') {
                            return null;
                        }
                        return {
                            language: node.getAttribute('data-language'),
                        };
                    },
                }],
            toDOM(node) {
                const { language = '' } = node.attrs;
                return [
                    'pre', {
                        'data-language': language,
                        spellcheck: 'false',
                    },
                    ['code', 0],
                ];
            },
        };
    }
    languageHandler(type) {
        this.nodeType = type;
        return (state, dispatch, view) => {
            const { selection } = state;
            if (!selection.empty) {
                return false;
            }
            const node = selection.$from.parent;
            if (node.type !== type) {
                return false;
            }
            const attrs = node.attrs;
            this.pluginView.setValues(attrs);
            this.pluginView.show(view);
            return true;
        };
    }
    getKeyMaps({ type, schema }) {
        return [{
                key: 'Mod-/',
                description: 'Change current block type to code',
                handler: block_1.toggleBlockType(type, schema.nodes.paragraph || type),
            }, {
                key: 'Mod-l',
                description: 'Change language of current code block (will work only if no text is selected)',
                handler: this.languageHandler(type),
            }];
    }
    getPlugins() {
        const that = this;
        return [
            new prosemirror_state_1.Plugin({
                view(editorView) {
                    return that.pluginView;
                },
            }),
        ];
    }
}
exports.default = Codeblock;
