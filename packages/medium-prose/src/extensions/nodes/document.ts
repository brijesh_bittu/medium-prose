import { IExtension, ExtensionType } from '../../extension/base';

export default class Text implements IExtension {
  type = ExtensionType.Node;
  name = 'doc';

  getSchema() {
    return {
      content: 'block+',
    };
  }
}
