import { MarkType, Node, Mark } from 'prosemirror-model';
import { TextSelection } from 'prosemirror-state';

import findParentPosFor from './findParentPosFor';

export default function findRangeOfMarkType(markType: MarkType, doc: Node, selection: TextSelection) {
  // if (!selection.empty) {
  //   return {
  //     selection,
  //     mark: null,
  //   };
  // }

  const { $from, from: cursorPos } = selection;
  const startLimitPos = $from.before($from.depth) + 1;
  const endLimitPos = $from.after($from.depth);

  let mark: Mark | null = null;
  let startPos = cursorPos;
  let endPos = cursorPos;

  for (;startPos >= startLimitPos;) {
    const node = doc.nodeAt(startPos);
    if (!node) {
      break;
    }

    const foundMark = markType.isInSet(node.marks);

    if (!node || !foundMark || (mark && foundMark !== mark)) {
      if (startPos !== cursorPos) {
        startPos++;
      }
      break;
    }

    startPos--;
    mark = foundMark;
  }

  for (;endPos <= endLimitPos;) {
    const node = doc.nodeAt(endPos);

    if (!node) {
      break;
    }

    const foundMark = markType.isInSet(node.marks);

    if (!foundMark || (mark && foundMark !== mark)) {
      break;
    }

    endPos++;
    mark = foundMark;
  }

  return {
    selection: selection.empty ? TextSelection.create(doc, startPos, endPos) : selection,
    mark,
  };
}
