"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_commands_1 = require("prosemirror-commands");
const base_1 = require("../extension/base");
class Underline {
    constructor() {
        this.type = base_1.ExtensionType.Mark;
        this.name = 'underline';
    }
    getSchema() {
        return {
            parseDOM: [
                { tag: 'u' },
            ],
            toDOM(mark, inline) {
                return ['u', 0];
            },
        };
    }
    getKeyMaps(options) {
        const { type } = options;
        return [{
                key: 'Mod-u',
                description: 'Underline selected text',
                handler: prosemirror_commands_1.toggleMark(type),
            }];
    }
}
exports.default = Underline;
