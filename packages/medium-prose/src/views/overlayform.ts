import { EditorView } from 'prosemirror-view';

import OverlayNode from './overlaynode';
import Field, { IFieldOptions } from './field';

export interface IFormOptions {
  fields: IFieldOptions[];
  showSubmitButton?: boolean;
  closeOnEscape?: boolean;
  onSubmit: (view: EditorView, values: {[key: string]: string}) => void;
  submitLabel?: string;
  ariaLabel?: string;
}

const OVERLAY_FORM_CLASS = 'Prosemirror__overlay-form';

let count = 0;

function getID() {
  const lastID = count++;
  return `Prosemirror__input-id__${lastID}`;
}

export default class OverlayForm extends OverlayNode {
  form: HTMLFormElement;
  fields: Field[];

  constructor(public options: IFormOptions) {
    super();

    this.form = document.createElement('form');

    if (options.ariaLabel) {
      this.form.setAttribute('aria-label', options.ariaLabel);
    }

    this.form.className = OVERLAY_FORM_CLASS;
    this.fields = options.fields.map(this.createField)
    this.dom.appendChild(this.form);
    this.fields.forEach(field => {
      this.form.appendChild(field.dom);
    });

    if (options.showSubmitButton) {
      this.form.appendChild(this.createSubmitButton());
    }

    this.form.addEventListener('submit', this.onSubmit);
  }

  private createField(fieldOptions: IFieldOptions) {
    return new Field(fieldOptions, getID());
  }

  private createSubmitButton() {
    const button = document.createElement('button');
    button.className = `${OVERLAY_FORM_CLASS}__submit-btn`;
    button.type = 'submit';
    button.textContent = this.options.submitLabel || 'Done';
    return button;
  }

  private onSubmit = (ev: Event) => {
    ev.preventDefault();
    const { onSubmit } = this.options;
    let isValid = true;
    const values: { [key: string]: string } = {};

    this.fields.forEach(field => {
      if (field.isValid()) {
        values[field.name] = field.value;
        return;
      }

      isValid = false;
      field.focus();
    });
    
    if (!isValid) {
      return;
    }

    onSubmit(this.editor, values);
  };

  private onKeyDown = (ev: KeyboardEvent) => {
    const { closeOnEscape } = this.options;

    if (ev.which === 27 && closeOnEscape) {
      this.hide();
    }
  }

  private onEditorFocus = () => {
    this.hide();
  };

  setValues(values: { [key: string]: string }) {
    this.fields.forEach(field => {
      const val = values[field.name];
      if (val !== undefined) {
        field.value = val;
      }
    });
  }

  getDom() {
    return super.dom;
  }

  update(view: EditorView) {
    this.setEditor(view);
  }

  show(view: EditorView) {
    this.setEditor(view);
    super.show();
    this.form.addEventListener('keydown', this.onKeyDown);
    this.editor.dom.addEventListener('focus', this.onEditorFocus);
    
    if (this.fields.length) {
      this.fields[0].focus();
    }
  }

  hide() {
    this.fields.forEach(f => {
      f.hideError();
      f.value = '';
    });
    this.form.removeEventListener('keydown', this.onKeyDown);
    this.editor.dom.removeEventListener('focus', this.onEditorFocus);
    super.hide();
  }

  destroy() {
    this.fields.forEach(field => field.destroy());
    this.form.removeEventListener('submit', this.onSubmit);
    this.form.removeEventListener('keydown', this.onKeyDown);
    this.editor.dom.removeEventListener('focus', this.onEditorFocus);
    this.dom.removeChild(this.form);
    super.destroy();
  }
}
