import {
  DOMOutputSpecArray,
  MarkType,
  NodeSpec,
  Node as ProsemirrorNode,
  NodeType,
} from 'prosemirror-model';

import { IExtension, ExtensionType } from '../../extension/base';
import { toggleWrap } from '../../commands/block';

export default class OrderedList implements IExtension {
  type = ExtensionType.Node;
  name = 'ordered_list';

  getSchema() {
    return {
      attrs: {
        order: {
          default: 1,
        },
      },
      content: 'list_item+',
      group: 'block',
      parseDOM: [{
        tag: 'ol',
        getAttrs(node: Node) {
          const element = node as Element;
          return {
            order: element.hasAttribute('start') ? Number(element.getAttribute('start')) : 1,
          };
        },
      }],
      toDOM(node) {
        return node.attrs.order == 1 ? ['ol', 0] : ['ol', {start: node.attrs.order}, 0];
      },
    } as NodeSpec;
  }

  getKeyMaps(options?: { type: MarkType | NodeType }) {
    const { type } = options!;
    return [{
      key: 'Mod-(',
      description: 'Wrap current text block in ordered list.',
      handler: toggleWrap(type as NodeType),
    }];
  }
}

