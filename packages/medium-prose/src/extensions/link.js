"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_state_1 = require("prosemirror-state");
const findRangeOfMarkType_1 = __importDefault(require("../utils/findRangeOfMarkType"));
const base_1 = require("../extension/base");
const overlayform_1 = __importDefault(require("../views/overlayform"));
class Link {
    constructor(options = {}) {
        this.type = base_1.ExtensionType.Mark;
        this.name = 'link';
        this.onSubmit = (editor, values) => {
            this.pluginView.hide();
            const { state } = editor;
            const { selection } = state;
            let tr = state.tr;
            let selectionToUse = selection;
            if (selection.empty) {
                const { selection: linkSelection } = findRangeOfMarkType_1.default(this.markType, state.doc, selection);
                selectionToUse = linkSelection;
            }
            if (selectionToUse.empty) {
                return;
            }
            if (!values.href) {
                tr = tr.removeMark(selectionToUse.from, selectionToUse.to, this.markType);
            }
            else {
                tr.addMark(selectionToUse.from, selectionToUse.to, this.markType.create(values));
            }
            if (tr === state.tr) {
                return;
            }
            editor.dispatch(tr);
        };
        this.pluginView = new overlayform_1.default({
            ariaLabel: 'Add or update link of the selected text',
            closeOnEscape: true,
            fields: [{
                    name: 'href',
                    type: 'text',
                    label: 'Enter URL (*)',
                    validate: options.validateLink ? options.validateLink : undefined,
                }, {
                    name: 'title',
                    type: 'text',
                    label: 'Title to add to the link',
                }],
            onSubmit: this.onSubmit,
            showSubmitButton: true,
        });
    }
    linkHandler(linkType, remove = false) {
        this.markType = linkType;
        return (state, dispatch, view) => {
            const { selection } = state;
            let { parent } = selection.$from;
            const { selection: linkSelection, mark } = findRangeOfMarkType_1.default(linkType, state.doc, selection);
            if (remove) {
                if (!mark || linkSelection.empty) {
                    return false;
                }
                const transaction = state.tr.removeMark(linkSelection.from, linkSelection.to, linkType);
                if (dispatch) {
                    dispatch(transaction);
                }
                return true;
            }
            if (!selection.empty || !linkSelection.empty) {
                if (mark) {
                    this.pluginView.setValues(mark.attrs);
                }
                this.pluginView.show(view);
                return true;
            }
            return false;
        };
    }
    getSchema() {
        const schema = {
            attrs: {
                href: {
                    default: null,
                },
                title: {
                    default: '',
                },
            },
            inclusive: false,
            parseDOM: [
                {
                    tag: 'a[href]',
                    getAttrs(node) {
                        if (typeof node === 'string') {
                            return null;
                        }
                        return {
                            href: node.getAttribute('href') || '',
                            title: node.getAttribute('title') || '',
                        };
                    },
                },
            ],
            toDOM(mark, inline) {
                return ['a', Object.assign(Object.assign({}, mark.attrs), { rel: 'noopener noreferrer nofollow' }), 0];
            },
        };
        return schema;
    }
    getKeyMaps(options) {
        const { type } = options;
        return [{
                key: 'Mod-k',
                description: 'Add link to selected text or update data if cursor is inside a link',
                handler: this.linkHandler(type),
            }, {
                key: 'Mod-Shift-K',
                description: 'Remove link from text if cursor is inside a link',
                handler: this.linkHandler(type, true),
            }];
    }
    getPlugins() {
        const that = this;
        return [
            new prosemirror_state_1.Plugin({
                view(editorView) {
                    return that.pluginView;
                },
                props: {
                    handleClick(view, pos, ev) {
                        if (!(ev.ctrlKey && ev.altKey)) {
                            return false;
                        }
                        const node = view.state.doc.nodeAt(pos);
                        if (!node || !node.marks.length) {
                            return false;
                        }
                        const mark = node.marks.find((m) => m.type === that.markType);
                        if (!mark) {
                            return false;
                        }
                        ev.preventDefault();
                        ev.stopPropagation();
                        window.open(mark.attrs.href);
                        return true;
                    }
                },
            }),
        ];
    }
}
exports.default = Link;
