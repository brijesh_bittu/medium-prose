import { MarkSpec, NodeSpec, Node } from 'prosemirror-model';

import {
  NODE_TYPE,
  BLOCK_TYPE,
  MARK_TYPE,
  zeroOrMore,
  oneOrMore,
} from 'medium-prose-plugins';

export const nodes: { [x: string]: NodeSpec } = {
  doc: {
    content: oneOrMore(NODE_TYPE.Block),
  },
  [BLOCK_TYPE.Paragraph]: {
    content: zeroOrMore(NODE_TYPE.Inline),
    group: NODE_TYPE.Block,
    parseDOM: [{ tag: 'p' }],
    toDOM() {
      return ['p', 0];
    },
  },
  [BLOCK_TYPE.BlockQuote]: {
    content: oneOrMore(NODE_TYPE.Block),
    group: NODE_TYPE.Block,
    defining: true,
    parseDOM: [{ tag: 'blockquote' }],
    toDOM() {
      return ['blockquote', 0];
    },
  },
  [BLOCK_TYPE.HR]: {
    content: NODE_TYPE.Block,
    parseDOM: [{ tag: 'hr' }],
    toDOM() {
      return ['hr'];
    },
  },
  [BLOCK_TYPE.Heading]: {
    attrs: {
      level: {
        default: 3,
      },
    },
    content: zeroOrMore(NODE_TYPE.Inline),
    group: NODE_TYPE.Block,
    defining: true,
    parseDOM: [
      { tag: 'h1', attrs: { level: 1 } },
      { tag: 'h2', attrs: { level: 2 } },
      { tag: 'h3', attrs: { level: 3 } },
      { tag: 'h4', attrs: { level: 4 } },
      { tag: 'h5', attrs: { level: 5 } },
      { tag: 'h6', attrs: { level: 6 } },
    ],
    toDOM(node: Node) {
      return [`h${node.attrs!.level}`, 0];
    },
  },
  [BLOCK_TYPE.CodeBlock]: {
    attrs: {
      lang: {
        default: '',
      },
    },
    content: zeroOrMore(NODE_TYPE.Text),
    marks: '',
    group: NODE_TYPE.Block,
    code: true,
    defining: true,
    parseDOM: [{
      tag: 'pre',
      preserveWhitespace: 'full',
      getAttrs(dom) {
        const domNode = dom as HTMLElement;
        return {
          lang: typeof dom === 'string' ? '' : domNode.getAttribute('data-lang') || '',
        };
      },
    }],
    toDOM(node: Node) {
      const { lang = '' } = node.attrs!;
      return ["pre", { 'data-lang': lang }, ["code", 0]];
    },
  },
  [BLOCK_TYPE.Text]: {
    group: NODE_TYPE.Inline,
  },
};

export const marks : { [key: string]: MarkSpec } = {
  [MARK_TYPE.Link]: {
    attrs: {
      href: {},
      title: { default: '' },
    },
    inclusive: true,
    parseDOM: [
      {
        tag: 'a[href]',
        getAttrs(dom) {
          const domNode = dom as HTMLAnchorElement;

          return {
            href: domNode.href,
            title: domNode.title,
          };
        },
      },
    ],
    toDOM(node) {
      const { href, title } = node.attrs;

      return ['a', { href, title }, 0];
    },
  },
  [MARK_TYPE.EM]: {
    parseDOM: [
      { tag: 'i' },
      { tag: 'em' },
      { style: 'font-style=italic' },
    ],
    toDOM() {
      return ['em', 0];
    },
  },
  [MARK_TYPE.Strong]: {
    parseDOM: [
      { tag: 'b' },
      { tag: 'strong' },
      { style: 'font-style=bold' },
    ],
    toDOM() {
      return ['strong', 0];
    },
  },
  [MARK_TYPE.Code]: {
    parseDOM: [
      { tag: 'code' },
    ],
    toDOM() {
      return ['code', 0];
    },
  },
};

