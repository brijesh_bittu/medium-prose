export const NODE_TYPE = {
  Block: 'block',
  Inline: 'inline',
  Text: 'text',
};

export const BLOCK_TYPE = {
  Paragraph: 'paragraph',
  BlockQuote: 'blockquote',
  HR: 'horizontal_rule',
  Heading: 'heading',
  CodeBlock: 'code_block',
  Text: 'text',
  LineBreak: 'line_break',
};

export const MARK_TYPE = {
  Link: 'link',
  EM: 'em',
  Strong: 'strong',
  Code: 'code',
};

export function oneOrMore(block: string) {
  return `(${block})+`;
}

export function zeroOrMore(block: string) {
  return `(${block})*`;
}

export function zeroOrOne(block: string) {
  return `(${block})?`;
}

export function anyOf(...args: string[]) {
  return args.join(' | ');
}

export const TOOLBAR_BASE_CLASS = 'Prosemirror__floating-toolbar';
