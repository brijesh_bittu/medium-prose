const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env, argv) => {
  const isProd = argv.mode === 'production';

  return {
    target: 'web',
    mode: 'development',
    entry: {
      demo: './demo/src/index.ts',
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js',
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.json', '.css'],
    },
    devtool: isProd ? 'source-map': 'inline-source-map',
    module: {
      rules: [{
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: 'ts-loader',
      }, {
        test: /\.css$/,
        use: (isProd ? [] : ['style-loader']).concat([
          'css-loader',
        ]),
      }],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.join(__dirname, './build/demo.template.html'),
      }),
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
    ],
  };
};
