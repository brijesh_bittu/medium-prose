"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_commands_1 = require("prosemirror-commands");
const base_1 = require("../../extension/base");
class Paragraph {
    constructor() {
        this.type = base_1.ExtensionType.Node;
        this.name = 'paragraph';
        this.isFallbackNode = true;
    }
    getSchema() {
        const schema = {
            content: 'inline*',
            group: 'block',
            draggable: false,
            parseDOM: [{
                    tag: 'p',
                }],
            toDOM() {
                return ['p', 0];
            },
        };
        return schema;
    }
    getKeyMaps(options) {
        const { type } = options;
        return [{
                key: 'Mod-G',
                description: `Toggle block type to paragraph`,
                handler: prosemirror_commands_1.setBlockType(type),
            }];
    }
}
exports.default = Paragraph;
