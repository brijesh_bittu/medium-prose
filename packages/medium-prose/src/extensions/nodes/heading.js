"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_commands_1 = require("prosemirror-commands");
const base_1 = require("../../extension/base");
class Heading {
    constructor(options = { levels: [1, 2, 3, 4, 5, 6] }) {
        this.options = options;
        this.type = base_1.ExtensionType.Node;
        this.name = 'heading';
    }
    getSchema() {
        const { levels } = this.options;
        return {
            attrs: {
                level: {
                    default: levels[0],
                },
            },
            content: 'inline*',
            group: 'block',
            defining: true,
            draggable: false,
            parseDOM: levels.map((level) => ({
                tag: `h${level}`,
                attrs: { level },
            })),
            toDOM(node) {
                const { level } = node.attrs;
                return [`h${level}`, 0];
            },
        };
    }
    getKeyMaps(options) {
        const { type } = options;
        const { levels } = this.options;
        return levels.map((level) => {
            return {
                key: `Mod-Shift-${level}`,
                description: `Toggle block type to h${level}`,
                handler: prosemirror_commands_1.setBlockType(type, { level }),
            };
        });
    }
}
exports.default = Heading;
