import { history, undo, redo } from 'prosemirror-history';
import { keymap } from 'prosemirror-keymap';
import { baseKeymap } from 'prosemirror-commands';

import { IExtension, ExtensionType } from './base';

export class HistoryPlugin implements IExtension {
  type = ExtensionType.Plugin;
  name = 'history';

  getKeyMaps() {
    return [{
      key: 'Mod-z',
      description: 'Undo last change',
      handler: undo,
    }, {
      key: 'Mod-y',
      description: 'Redo last change',
      handler: redo,
    }];
  }

  getPlugins() {
    return [
      history(),
    ]
  }
}

export class BaseKeymapPlugin implements IExtension {
  type = ExtensionType.Plugin;
  name = 'base_keymap';

  getPlugins() {
    return [
      keymap(baseKeymap),
    ];
  }
}
