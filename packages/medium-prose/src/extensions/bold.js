"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_commands_1 = require("prosemirror-commands");
const base_1 = require("../extension/base");
class Bold {
    constructor() {
        this.type = base_1.ExtensionType.Mark;
        this.name = 'bold';
    }
    getSchema() {
        return {
            parseDOM: [
                { tag: 'b' },
                { tag: 'strong' },
                { style: 'font-style=bold' },
            ],
            toDOM(mark, inline) {
                return ['strong', 0];
            },
        };
    }
    getKeyMaps(options) {
        const type = options.type;
        return [{
                key: 'Mod-b',
                description: 'Toggle the selection to/from bold',
                handler: prosemirror_commands_1.toggleMark(type),
            }];
    }
}
exports.default = Bold;
