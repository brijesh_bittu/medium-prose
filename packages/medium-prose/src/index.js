"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const manager_1 = __importDefault(require("./extension/manager"));
const document_1 = __importDefault(require("./extensions/nodes/document"));
const text_1 = __importDefault(require("./extensions/nodes/text"));
const paragraph_1 = __importDefault(require("./extensions/nodes/paragraph"));
const blockquote_1 = __importDefault(require("./extensions/nodes/blockquote"));
const code_1 = __importDefault(require("./extensions/nodes/code"));
const heading_1 = __importDefault(require("./extensions/nodes/heading"));
const bold_1 = __importDefault(require("./extensions/bold"));
const italic_1 = __importDefault(require("./extensions/italic"));
const underline_1 = __importDefault(require("./extensions/underline"));
const link_1 = __importDefault(require("./extensions/link"));
const code_2 = __importDefault(require("./extensions/code"));
exports.manager = new manager_1.default({
    extensions: [
        new document_1.default(),
        new text_1.default(),
        new paragraph_1.default(),
        new heading_1.default(),
        new blockquote_1.default(),
        new code_1.default(),
        new link_1.default({
            validateLink(value) {
                // useless validation for demo purposes
                if (!value.startsWith('http')) {
                    return 'Enter a valid URL.';
                }
                return null;
            }
        }),
        new bold_1.default(),
        new italic_1.default(),
        new underline_1.default(),
        new code_2.default(),
    ],
});
