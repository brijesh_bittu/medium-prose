"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class OverlayNode {
    constructor() {
        this.attached = false;
        this.dom = document.createElement('div');
        this.dom.className = 'Prosemirror__overlay-node';
        this.dom.style.position = 'absolute';
    }
    setEditor(editor) {
        this.editor = editor;
    }
    attach() {
        if (this.attached || !this.editor) {
            return;
        }
        this.attached = true;
        this.editor.dom.parentElement.appendChild(this.dom);
    }
    adjustPosition() {
        const { editor } = this;
        const { selection } = editor.state;
        const { $from, from, to, anchor } = selection;
        const node = $from.node($from.depth);
        const wrapperCoords = editor.dom.parentElement.getBoundingClientRect();
        const boxCoords = this.dom.getBoundingClientRect();
        const startCoords = editor.coordsAtPos(from);
        const endCoords = editor.coordsAtPos(to);
        const cursorCoords = editor.coordsAtPos(anchor);
        const left = Math.abs(wrapperCoords.left - startCoords.left) + Math.abs(endCoords.left - startCoords.left) / 2 - boxCoords.width / 2;
        // console.log({ wrapperCoords, boxCoords, cursorCoords, startCoords, endCoords });
        this.dom.style.top = `${cursorCoords.top - wrapperCoords.top + OverlayNode.OFFSET_HEIGHT}px`;
        this.dom.style.left = `${left}px`;
    }
    show(...args) {
        this.attach();
        this.dom.style.display = 'initial';
        this.adjustPosition();
    }
    hide() {
        this.dom.style.display = 'none';
        this.editor.focus();
        this.editor.dispatch(this.editor.state.tr.scrollIntoView());
    }
    detach() {
        if (!this.attached) {
            return;
        }
        this.dom.parentElement.removeChild(this.dom);
        this.attached = false;
    }
    destroy() {
        this.detach();
    }
}
exports.default = OverlayNode;
OverlayNode.OFFSET_HEIGHT = 30;
