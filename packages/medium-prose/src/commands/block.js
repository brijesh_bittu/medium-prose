"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_utils_1 = require("prosemirror-utils");
const prosemirror_commands_1 = require("prosemirror-commands");
function isNodeActive(state, type, attrs) {
    const predicate = (node) => node.type === type;
    const node = prosemirror_utils_1.findSelectedNodeOfType(type)(state.selection)
        || prosemirror_utils_1.findParentNode(predicate)(state.selection);
    if (!node || !attrs || !Object.keys(attrs).length) {
        return !!node;
    }
    return node.node.hasMarkup(type, attrs);
}
exports.isNodeActive = isNodeActive;
function toggleWrap(type) {
    return function (state, dispatch) {
        const isActive = isNodeActive(state, type);
        if (isActive) {
            return prosemirror_commands_1.lift(state, dispatch);
        }
        return prosemirror_commands_1.wrapIn(type)(state, dispatch);
    };
}
exports.toggleWrap = toggleWrap;
function toggleBlockType(type, toggleType, attrs = {}) {
    return function (state, dispatch) {
        const isActive = isNodeActive(state, type);
        if (isActive) {
            return prosemirror_commands_1.setBlockType(toggleType)(state, dispatch);
        }
        return prosemirror_commands_1.setBlockType(type, attrs)(state, dispatch);
    };
}
exports.toggleBlockType = toggleBlockType;
