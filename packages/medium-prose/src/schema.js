"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const medium_prose_plugins_1 = require("medium-prose-plugins");
exports.nodes = {
    doc: {
        content: medium_prose_plugins_1.oneOrMore(medium_prose_plugins_1.NODE_TYPE.Block),
    },
    [medium_prose_plugins_1.BLOCK_TYPE.Paragraph]: {
        content: medium_prose_plugins_1.zeroOrMore(medium_prose_plugins_1.NODE_TYPE.Inline),
        group: medium_prose_plugins_1.NODE_TYPE.Block,
        parseDOM: [{ tag: 'p' }],
        toDOM() {
            return ['p', 0];
        },
    },
    [medium_prose_plugins_1.BLOCK_TYPE.BlockQuote]: {
        content: medium_prose_plugins_1.oneOrMore(medium_prose_plugins_1.NODE_TYPE.Block),
        group: medium_prose_plugins_1.NODE_TYPE.Block,
        defining: true,
        parseDOM: [{ tag: 'blockquote' }],
        toDOM() {
            return ['blockquote', 0];
        },
    },
    [medium_prose_plugins_1.BLOCK_TYPE.HR]: {
        content: medium_prose_plugins_1.NODE_TYPE.Block,
        parseDOM: [{ tag: 'hr' }],
        toDOM() {
            return ['hr'];
        },
    },
    [medium_prose_plugins_1.BLOCK_TYPE.Heading]: {
        attrs: {
            level: {
                default: 3,
            },
        },
        content: medium_prose_plugins_1.zeroOrMore(medium_prose_plugins_1.NODE_TYPE.Inline),
        group: medium_prose_plugins_1.NODE_TYPE.Block,
        defining: true,
        parseDOM: [
            { tag: 'h1', attrs: { level: 1 } },
            { tag: 'h2', attrs: { level: 2 } },
            { tag: 'h3', attrs: { level: 3 } },
            { tag: 'h4', attrs: { level: 4 } },
            { tag: 'h5', attrs: { level: 5 } },
            { tag: 'h6', attrs: { level: 6 } },
        ],
        toDOM(node) {
            return [`h${node.attrs.level}`, 0];
        },
    },
    [medium_prose_plugins_1.BLOCK_TYPE.CodeBlock]: {
        attrs: {
            lang: {
                default: '',
            },
        },
        content: medium_prose_plugins_1.zeroOrMore(medium_prose_plugins_1.NODE_TYPE.Text),
        marks: '',
        group: medium_prose_plugins_1.NODE_TYPE.Block,
        code: true,
        defining: true,
        parseDOM: [{
                tag: 'pre',
                preserveWhitespace: 'full',
                getAttrs(dom) {
                    const domNode = dom;
                    return {
                        lang: typeof dom === 'string' ? '' : domNode.getAttribute('data-lang') || '',
                    };
                },
            }],
        toDOM(node) {
            const { lang = '' } = node.attrs;
            return ["pre", { 'data-lang': lang }, ["code", 0]];
        },
    },
    [medium_prose_plugins_1.BLOCK_TYPE.Text]: {
        group: medium_prose_plugins_1.NODE_TYPE.Inline,
    },
};
exports.marks = {
    [medium_prose_plugins_1.MARK_TYPE.Link]: {
        attrs: {
            href: {},
            title: { default: '' },
        },
        inclusive: true,
        parseDOM: [
            {
                tag: 'a[href]',
                getAttrs(dom) {
                    const domNode = dom;
                    return {
                        href: domNode.href,
                        title: domNode.title,
                    };
                },
            },
        ],
        toDOM(node) {
            const { href, title } = node.attrs;
            return ['a', { href, title }, 0];
        },
    },
    [medium_prose_plugins_1.MARK_TYPE.EM]: {
        parseDOM: [
            { tag: 'i' },
            { tag: 'em' },
            { style: 'font-style=italic' },
        ],
        toDOM() {
            return ['em', 0];
        },
    },
    [medium_prose_plugins_1.MARK_TYPE.Strong]: {
        parseDOM: [
            { tag: 'b' },
            { tag: 'strong' },
            { style: 'font-style=bold' },
        ],
        toDOM() {
            return ['strong', 0];
        },
    },
    [medium_prose_plugins_1.MARK_TYPE.Code]: {
        parseDOM: [
            { tag: 'code' },
        ],
        toDOM() {
            return ['code', 0];
        },
    },
};
