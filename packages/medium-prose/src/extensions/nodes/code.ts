import {
  DOMOutputSpecArray,
  MarkType,
  NodeSpec,
  NodeType,
  Node as ProsemirrorNode,
} from 'prosemirror-model';
import { Transaction, EditorState, Plugin } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import { setBlockType } from 'prosemirror-commands';

import { IKeymapOptions, IExtension, ExtensionType } from '../../extension/base';
import { toggleBlockType } from '../../commands/block';
import findParentPosFor from '../../utils/findParentPosFor';
import OverlayForm from '../../views/overlayform';

export default class Codeblock implements IExtension {
  pluginView: OverlayForm;
  nodeType: NodeType;

  type = ExtensionType.Node;
  name = 'code';

  constructor() {
    this.pluginView = new OverlayForm({
      fields: [{
        type: 'text',
        name: 'language',
        label: 'Set language for current code block',
      }],
      closeOnEscape: true,
      onSubmit: this.onSubmit,
    });
  }

  onSubmit = (view: EditorView, values: { [key: string]: string }) => {
    const { state, dispatch } = view;
    const { $from } = state.selection;
    const pos = findParentPosFor($from, (node: ProsemirrorNode) => node.type === this.nodeType);

    if (pos !== undefined) {
      dispatch(state.tr.setNodeMarkup(pos, undefined, values));
    }

    this.pluginView.hide();
  };

  getSchema() {
    return {
      content: 'text*',
      marks: '',
      group: 'block',
      code: true,
      defining: true,
      draggable: false,
      attrs: {
        language: {
          default: '',
        },
      },
      parseDOM: [{
        tag: 'pre[data-language]',
        preserveWhitespace: 'full',
        getAttrs(node: string | Node) {
          if (typeof node === 'string') {
            return null;
          }

          return {
            language: (node as Element).getAttribute('data-language'),
          };
        },
      }],
      toDOM(node: ProsemirrorNode) {
        const { language = '' } = node.attrs!;
        return [
          'pre', {
            'data-language': language,
            spellcheck: 'false',
          },
          ['code', 0],
        ];
      },
    } as NodeSpec;
  }

  languageHandler(type: NodeType) {
    this.nodeType = type;

    return (state: EditorState, dispatch?: (tr: Transaction) => void, view?: EditorView) => {
      const { selection } = state;
      if (!selection.empty) {
        return false;
      }

      const node = selection.$from.parent;
      if (node.type !== type) {
        return false;
      }

      const attrs = node.attrs;
      this.pluginView.setValues(attrs);
      this.pluginView.show(view!);
      return true;
    }
  }

  getKeyMaps({ type, schema }: IKeymapOptions) {
    return [{
      key: 'Mod-/',
      description: 'Change current block type to code',
      handler: toggleBlockType(type as NodeType, schema.nodes.paragraph || type),
    }, {
      key: 'Mod-l',
      description: 'Change language of current code block (will work only if no text is selected)',
      handler: this.languageHandler(type as NodeType),
    }];
  }

  getPlugins() {
    const that = this;
    return [
      new Plugin({
        view(editorView: EditorView) {
          return that.pluginView;
        },
      }),
    ];
  }
}

