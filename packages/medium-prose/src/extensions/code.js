"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_commands_1 = require("prosemirror-commands");
const base_1 = require("../extension/base");
class Code {
    constructor() {
        this.type = base_1.ExtensionType.Mark;
        this.name = 'inline_code';
    }
    getSchema() {
        return {
            parseDOM: [
                { tag: 'code' },
            ],
            toDOM(mark, inline) {
                return ['code', 0];
            },
        };
    }
    getKeyMaps(options) {
        const type = options.type;
        return [{
                key: 'Mod-`',
                description: 'Toggle selected text to monospaced text.',
                handler: prosemirror_commands_1.toggleMark(type),
            }];
    }
}
exports.default = Code;
