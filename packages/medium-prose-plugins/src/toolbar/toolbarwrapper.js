"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const preact_1 = require("preact");
const prosemirror_state_1 = require("prosemirror-state");
const constants_1 = require("../constants");
const toolbar_1 = __importDefault(require("./toolbar"));
const pluginKey = new prosemirror_state_1.PluginKey('floating-toolbar-plugin');
class ToolbarState {
    init(config, state) {
        return false;
    }
    apply(tr, value, oldState, newState) {
        if (newState.selection.eq(oldState.selection) || newState.selection.empty) {
            return false;
        }
        return true;
    }
}
class ToolbarWrapper {
    constructor(key, view) {
        this.renderedElement = null;
        this.view = view;
        this.key = key;
        this.container = document.createElement('div');
        this.container.className = `${constants_1.TOOLBAR_BASE_CLASS}__container`;
    }
    update(view, prevState) {
        const { state } = view;
        const newState = this.key.getState(state);
        const oldState = this.key.getState(prevState);
        if (newState) {
            this.show();
        }
        else {
            this.show(false);
        }
    }
    show(show = true) {
        if (!this.renderedElement) {
            this.view.dom.parentElement.appendChild(this.container);
        }
        const { schema } = this.view.state;
        const props = {
            show,
            schema,
        };
        if (!this.renderedElement) {
            this.renderedElement = preact_1.render(preact_1.h(toolbar_1.default, props), this.container);
        }
        else {
            this.renderedElement = preact_1.render(preact_1.h(toolbar_1.default, props), this.container, this.renderedElement);
        }
    }
    destroy() {
        if (this.renderedElement) {
            preact_1.render(null, this.container, this.renderedElement);
            this.renderedElement = null;
        }
        this.view.dom.parentElement.removeChild(this.container);
    }
}
exports.toolbarPlugin = new prosemirror_state_1.Plugin({
    key: pluginKey,
    state: new ToolbarState(),
    view(editorView) {
        return new ToolbarWrapper(pluginKey, editorView);
    },
});
