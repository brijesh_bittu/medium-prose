"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_commands_1 = require("prosemirror-commands");
const base_1 = require("../extension/base");
class Italic {
    constructor() {
        this.type = base_1.ExtensionType.Mark;
        this.name = 'italic';
    }
    getSchema() {
        return {
            parseDOM: [
                { tag: 'i' },
                { tag: 'em' },
                { style: 'font-style=italic' },
            ],
            toDOM(mark, inline) {
                return ['em', 0];
            },
        };
    }
    getKeyMaps(options) {
        const { type } = options;
        return [{
                key: 'Mod-i',
                description: 'Toggle the selection to/from italic',
                handler: prosemirror_commands_1.toggleMark(type),
            }];
    }
}
exports.default = Italic;
