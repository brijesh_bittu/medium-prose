"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prosemirror_history_1 = require("prosemirror-history");
const prosemirror_keymap_1 = require("prosemirror-keymap");
const prosemirror_commands_1 = require("prosemirror-commands");
const base_1 = require("./base");
class HistoryPlugin {
    constructor() {
        this.type = base_1.ExtensionType.Plugin;
        this.name = 'history';
    }
    getKeyMaps() {
        return [{
                key: 'Mod-z',
                description: 'Undo last change',
                handler: prosemirror_history_1.undo,
            }, {
                key: 'Mod-y',
                description: 'Redo last change',
                handler: prosemirror_history_1.redo,
            }];
    }
    getPlugins() {
        return [
            prosemirror_history_1.history(),
        ];
    }
}
exports.HistoryPlugin = HistoryPlugin;
class BaseKeymapPlugin {
    constructor() {
        this.type = base_1.ExtensionType.Plugin;
        this.name = 'base_keymap';
    }
    getPlugins() {
        return [
            prosemirror_keymap_1.keymap(prosemirror_commands_1.baseKeymap),
        ];
    }
}
exports.BaseKeymapPlugin = BaseKeymapPlugin;
