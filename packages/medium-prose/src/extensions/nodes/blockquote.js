"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = require("../../extension/base");
const block_1 = require("../../commands/block");
class Blockquote {
    constructor() {
        this.type = base_1.ExtensionType.Node;
        this.name = 'blockquote';
    }
    getSchema() {
        return {
            content: 'block*',
            group: 'block',
            defining: true,
            draggable: false,
            parseDOM: [{
                    tag: 'blockquote',
                }],
            toDOM() {
                return ['blockquote', 0];
            },
        };
    }
    getKeyMaps(options) {
        const { type } = options;
        return [{
                key: 'Mod->',
                description: 'Toggle block type to Blockquote',
                handler: block_1.toggleWrap(type),
            }];
    }
}
exports.default = Blockquote;
