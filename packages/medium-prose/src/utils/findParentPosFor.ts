import { Node, ResolvedPos } from 'prosemirror-model';

export default function findParentPosFor($position: ResolvedPos, predicate: (node: Node) => boolean): number | undefined {
  for (let i = $position.depth; i > 0; i--) {
    const node = $position.node(i);
    if (predicate(node)) {
      return i > 0 ? $position.before(i) : 0;
    }
  }
}

