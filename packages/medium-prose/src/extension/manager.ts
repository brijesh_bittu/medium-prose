import { Schema, MarkSpec, NodeSpec } from 'prosemirror-model';
import { EditorState, Plugin } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import { keymap } from 'prosemirror-keymap';

import { IExtension, ExtensionType, KeyHandler } from './base';
import { HistoryPlugin, BaseKeymapPlugin } from './baseplugin';

interface IOptions {
  extensions?: IExtension[];
  useDefaultExtensions?: boolean;
}

const defaultOptions: IOptions = {
  useDefaultExtensions: true,
  extensions: [],
};

export default class ExtensionManager {
  schema: Schema;
  state: EditorState;
  fallbackNode?: NodeSpec;
  editor?: EditorView;
  keyMapDescriptions: { key: string, description: string }[] = [];
  keyMaps: {
    [key: string]: KeyHandler;
  } = {};

  constructor(public options: IOptions = defaultOptions) {
    this.init();
  }

  private init() {
    const marks: { [key: string]: MarkSpec } = {};
    const nodes: { [key: string]: NodeSpec } = {};
    const plugins: Plugin[] = [];

    const defaultExtensions: IExtension[] = [
      new BaseKeymapPlugin(),
      new HistoryPlugin(),
    ];
    const { extensions } = this.options;
    const fullList = defaultExtensions.concat(extensions ? extensions : []);

    fullList.forEach((extension) => {
      const markSpec = this.getMarks(extension);

      if (markSpec) {
        marks[extension.name] = markSpec;
      }

      const nodeSpec = this.getNodes(extension);

      if (nodeSpec) {
        nodes[extension.name] = nodeSpec;
      }

      const tmpPlugins = this.getPlugins(extension);

      if (tmpPlugins && tmpPlugins.length) {
        plugins.push(...tmpPlugins);
      }
    });

    this.schema = new Schema({
      marks,
      nodes,
    });

    this.createKeyMaps(this.schema, fullList);

    this.state = EditorState.create({
      schema: this.schema,
      plugins: [
        ...plugins,
        keymap(this.keyMaps),
      ],
    });
  }

  private getMarks(extension: IExtension): null | MarkSpec {
    if (extension.type !== ExtensionType.Mark) {
      return null;
    }

    const spec = extension.getSchema && extension.getSchema();

    if (!spec) {
      return null;
    }

    return spec as MarkSpec;
  }

  private getNodes(extension: IExtension): null | NodeSpec {
    if (extension.type !== ExtensionType.Node) {
      return null;
    }

    const spec = extension.getSchema && extension.getSchema();

    if (!spec) {
      return null;
    }

    return spec as NodeSpec;
  }

  private getPlugins(extension: IExtension): null | Plugin[] {
    const plugins = extension.getPlugins && extension.getPlugins();

    if (!plugins || !plugins.length) {
      return null;
    }

    return plugins;
  }

  private createKeyMaps(schema: Schema, extensions: IExtension[]) {
    extensions.forEach((extension) => {
      let keyMaps;

      if (extension.type === ExtensionType.Plugin) {
        keyMaps = extension.getKeyMaps && extension.getKeyMaps();
      } else {
        const key = extension.type === ExtensionType.Node ? 'nodes' : 'marks';
        const extensionSchema = schema[key][extension.name];
        keyMaps = extension.getKeyMaps && extension.getKeyMaps({ type: extensionSchema, schema });
      }


      if (!keyMaps || !keyMaps.length) {
        return;
      }

      keyMaps.forEach((keyMap) => {
        this.keyMaps[keyMap.key] = keyMap.handler;
        this.keyMapDescriptions.push({
          key: keyMap.key,
          description: keyMap.description || 'No description',
        });
      });
    });
  }

  mount(dom: HTMLElement) {
    const editor = new EditorView(dom, {
      state: this.state,
    });

    this.editor = editor;
    editor.setProps({
      state: this.state,
      attributes: {
        'aria-label': 'Press Mod-? to see all available shortcuts',
      },
    });
    return editor;
  }
}

