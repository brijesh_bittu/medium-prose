import { EditorView } from 'prosemirror-view';

import ExtensionManager from './extension/manager';

import Document from './extensions/nodes/document';
import Text from './extensions/nodes/text';
import Paragraph from './extensions/nodes/paragraph';
import Blockquote from './extensions/nodes/blockquote';
import Codeblock from './extensions/nodes/code';
import Heading from './extensions/nodes/heading';

import Bold from './extensions/bold';
import Italic from './extensions/italic';
import Underline from './extensions/underline';
import Link from './extensions/link';
import Code from './extensions/code';

export const manager = new ExtensionManager({
  extensions: [
    new Document(),
    new Text(),
    new Paragraph(),
    new Heading(),
    new Blockquote(),
    new Codeblock(),
    new Link({
      validateLink(value) {
        // useless validation for demo purposes
        if (!value.startsWith('http')) {
          return 'Enter a valid URL.';
        }

        return null;
      }
    }),
    new Bold(),
    new Italic(),
    new Underline(),
    new Code(),
  ],
});

