"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = require("../../extension/base");
const block_1 = require("../../commands/block");
class OrderedList {
    constructor() {
        this.type = base_1.ExtensionType.Node;
        this.name = 'ordered_list';
    }
    getSchema() {
        return {
            attrs: {
                order: {
                    default: 1,
                },
            },
            content: 'list_item+',
            group: 'block',
            parseDOM: [{
                    tag: 'ol',
                    getAttrs(node) {
                        const element = node;
                        return {
                            order: element.hasAttribute('start') ? Number(element.getAttribute('start')) : 1,
                        };
                    },
                }],
            toDOM(node) {
                return node.attrs.order == 1 ? ['ol', 0] : ['ol', { start: node.attrs.order }, 0];
            },
        };
    }
    getKeyMaps(options) {
        const { type } = options;
        return [{
                key: 'Mod-(',
                description: 'Wrap current text block in ordered list.',
                handler: block_1.toggleWrap(type),
            }];
    }
}
exports.default = OrderedList;
